# Desafio para o Módulo Chatbot

Bem vindo(a) ao desafio para o módulo Chatbot!

Neste desafio, você precisará corrigir e aprimorar uma aplicação que envolve o SAC do Mateus. Foram colocados alguns poucos erros propositais dentro do código, então não se assuste se o projeto não rodar de primeira, faz parte do processo :)

A principal razão deste desafio existir é para que nós possamos ter uma ideia do quão bom você é em programação, então construímos um desafio à sua altura. O principal é que você mostre sua forma de programar, então fique tranquilo caso ao final do prazo a aplicação não estiver 100%, o importante é você enviar para nós o que foi feito durante este tempo de desafio.

Elaboramos uma lista de critérios que gostaríamos que fossem entregues, além de uma lista de critérios opcionais que, se entregues em conjunto com os principais, facilitarão bastante nossa análise.

## Considerações

Solicitamos que utilize o MongoDB como banco de dados.

Para começar o desafio, basta fazer um fork do repositório (o link está na seção de Recursos) e começar a codificar.

Em cada subprojeto, disponibilizamos algumas bibliotecas que comumente utilizamos em nosso dia-a-dia. Isto não lhe impede de adicionar outras que você achar necessárias para o desenvolvimento, assim como não há problema em não utilizar todas as bibliotecas que disponibilizamos.

Ao terminar o que você quer entregar, faça um PR de volta para o repositório original e mostre-nos (de preferência na documentação) como executar o projeto para vermos as novas mudanças. Quanto mais simples, melhor. Afinal, somos seus clientes :)

Logo abaixo, escrevemos alguns requisitos que representam o MVP (Minimal Viable Product) desta aplicação, em conjunto com algumas referências à lista de objetivos opcionais. Por exemplo, se um trecho possuir `OO5` em seu final, quer dizer que este requisito pode ser opcionalmente implementado da maneira que o Objetivo Opcional número 5 descreve.

Sinta-se à vontade para priorizar quaisquer objetivos opcionais. Eles foram enumerados e citados em ordem apenas por fins de organização.

## Requisitos da Aplicação

Quando um cliente quer entrar no Serviço de Atendimento ao Consumidor `OO1 e OO2`, ele precisa inserir seus dados `OO3` para que o atendente possa iniciar o atendimento quando estiver disponível.

Assim que o atendente estiver pronto `OO4`, ele deverá observar as informações de cada cliente para que possa analisar qual deles é mais relevante e, em seguida, iniciar o atendimento `OO5`.

Assim que o atendente julgar que o atendimento foi concluído `OO6`, ele deverá finalizá-lo, impossibilitando a continuação do atendimento nesta sessão `OO7`.

## Objetivos Principais

- Consertar os erros críticos já existentes na aplicação;
- Aprimorar a legibilidade da aplicação;
- O gerenciamento de dados externos da aplicação deve sempre ser intermediado pelo back-end;
- Simule a realização do atendimento através do clique de um botão;
- Ter os requisitos básicos já descritos;
- Aplicação de princípios RESTful.

## Objetivos Opcionais

1. Um sistema de login que possa diferenciar atendente e cliente;
2. Apresentar um FAQ para o cliente;
3. Utilizar os dados pessoais salvos do cliente para que seja solicitado apenas o assunto no próximo atendimento;
4. Suporte para que o atendimento possa ser realizado por diferentes atendentes;
5. Diferenciação entre conversas iniciadas e finalizadas;
6. Troca de mensagens em tempo real entre o cliente e o atendente;
7. Desenvolver para diferentes ambientes (local, homologação e produção);
8. Testes de código;
9. Responsividade do layout em diferentes resoluções;
10. Aplicação de princípios básicos de usabilidade no front-end.

## Recursos

[Repositório do projeto](https://gitlab.com/vitorthomaz/programming-challenge)

[Mock-ups](https://www.figma.com/file/USQ8npKcyNKNc7z7cjOTxa/TESTE-%2F-CHATBOT?node-id=7%3A211)
